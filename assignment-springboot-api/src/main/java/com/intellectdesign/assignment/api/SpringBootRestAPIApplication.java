package com.intellectdesign.assignment.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestAPIApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestAPIApplication.class, args);
	}
}
