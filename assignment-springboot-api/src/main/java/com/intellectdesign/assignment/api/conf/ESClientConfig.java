package com.intellectdesign.assignment.api.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestClientFactory;
import io.searchbox.client.config.HttpClientConfig;

@Configuration
public class ESClientConfig {
	
	@Value("${elasticsearch.host}")
	private String esUrl;
	
	@Value("${elasticsearch.port}")
	private int esPort;
	
	JestClient jestClient = null;
	
	
	@Bean
	public JestClient getJestClient() {
		JestClientFactory factory = new JestClientFactory();
		 
	        factory.setHttpClientConfig(new HttpClientConfig
	        		.Builder("http://"+ esUrl + ":"+ esPort)
	                .multiThreaded(true)
	                .build());
	        jestClient = factory.getObject();
		return jestClient;
	}
	

	public void close(){
		jestClient.shutdownClient();
	}
	
	
	
	
}
