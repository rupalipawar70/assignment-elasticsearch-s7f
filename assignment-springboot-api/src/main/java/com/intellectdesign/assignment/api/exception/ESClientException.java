package com.intellectdesign.assignment.api.exception;

public class ESClientException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6085565441117358798L;

	public ESClientException(String message, Throwable cause) {
		super(message, cause);
	}

	public ESClientException(String message) {
		super(message);
	}
}
