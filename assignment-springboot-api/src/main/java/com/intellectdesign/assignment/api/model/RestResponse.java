package com.intellectdesign.assignment.api.model;

import java.util.List;

public class RestResponse {

	private String resMsg;
	private String userId;
	private List<RestError> valErrors;
	
	public String getResMsg() {
		return resMsg;
	}
	public void setResMsg(String resMsg) {
		this.resMsg = resMsg;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public List<RestError> getValErrors() {
		return valErrors;
	}
	public void setValErrors(List<RestError> valErrors) {
		this.valErrors = valErrors;
	}
}
