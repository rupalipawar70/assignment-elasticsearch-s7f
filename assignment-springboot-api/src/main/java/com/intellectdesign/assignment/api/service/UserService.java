package com.intellectdesign.assignment.api.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.intellectdesign.assignment.api.exception.ESClientException;
import com.intellectdesign.assignment.api.exception.InvalidDataException;
import com.intellectdesign.assignment.api.model.RestError;
import com.intellectdesign.assignment.api.model.RestResponse;
import com.intellectdesign.assignment.api.model.User;
import com.intellectdesign.assignment.api.util.APICommonUtility;
import com.intellectdesign.assignment.api.util.ESUtil;
import com.intellectdesign.assignment.api.util.RandomIDGenerator;

import io.searchbox.client.JestResult;
import io.searchbox.core.SearchResult;

@Service
public class UserService {
	
//	public static HashMap<String,User> hmUser = new HashMap<String,User>();
//	public static List<String> emailIdList = new ArrayList<String>();
	
	
	@Autowired
	RandomIDGenerator randomIDUtil;
	@Autowired
	APICommonUtility apiCommonUtil;
	@Autowired
	ESUtil esUtil;
	@Autowired
	APICommonUtility apiCommonUtility;
	@Autowired
	ObjectMapper objectMapper;
	@Autowired
	Gson gson;
	
	public User createUser(User user) throws InvalidDataException, IOException, ESClientException {
		
		SearchResult searchResult = esUtil.searchIntoESData(user.getEmail());
		if(isEmailExist(searchResult)) {
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(), 
					"email", 
					"User email is already exist.",
					user.getId());
			throw new InvalidDataException(exceptionResponse);
		}
		
		String userId = randomIDUtil.generateRandomId();
			user.setId(userId);
			user.setActive(true);
//		hmUser.put(userId, user);
//		emailIdList.add(user.getEmail());
		
		String jsondata = apiCommonUtility.getJsonStringFromObject(user);
		//Store data into ES
		JestResult jestResult = esUtil.processESData("create", jsondata, userId);
		if (jestResult.getResponseCode() != 201) 
			return null;
		return user;
	}
	
	public User updateUser(User user,String userId)throws InvalidDataException, IOException, ESClientException {
		
		//Read data from ES
		JestResult result = esUtil.processESData("read", null, userId);
		JsonObject userObj = result.getJsonObject().getAsJsonObject("_source");

		String userJsonStr =  gson.toJson(userObj);
		User oldUser =  new GsonBuilder()
			        .setDateFormat("dd-MMM-yyyy")
			        .create()
			        .fromJson(userJsonStr, User.class);
		
		if(oldUser.getId() == null) {
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(), 
					"id",
					"Invalid userId.",
					userId);
			throw new InvalidDataException(exceptionResponse);
		}
		if(!oldUser.isActive()) {
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(), 
					"id",
					"You can not modify data of deactivated user.",
					userId);
			throw new InvalidDataException(exceptionResponse);
		}
		oldUser.setBirthDate(user.getBirthDate());
		oldUser.setPinCode(user.getPinCode());
			
//		hmUser.put(oldUser.getId(), oldUser);
		String jsonData = apiCommonUtility.getJsonStringFromObject(oldUser);
		//Update data into ES
		String updateData = "{\"doc\":"+jsonData +"}";
		JestResult jestResult = esUtil.processESData("update", updateData, userId);
		if (jestResult.getResponseCode() != 200) 
			return null;
		return oldUser;
	}
	
	public void disableUser(String userId) throws InvalidDataException, IOException, ESClientException {
		
		//Read data from ES
		JestResult result = esUtil.processESData("read", null, userId);
		JsonObject userObj = result.getJsonObject().getAsJsonObject("_source");
		
		String userJsonStr =  gson.toJson(userObj);
		User user =  new GsonBuilder()
			        .setDateFormat("dd-MMM-yyyy")
			        .create().fromJson(userJsonStr, User.class);
		if(user == null) {
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(), 
					"id",
					"Invalid userId.",
					userId);
			throw new InvalidDataException(exceptionResponse);
		}
			
		if(!user.isActive()) {
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(), 
					"id",
					"User is already deactivated.",
					userId);
			throw new InvalidDataException(exceptionResponse);
		}
		user.setActive(false);
//		hmUser.put(user.getId(), user);
		//Delete data from ES
		JestResult jestResult = esUtil.processESData("delete", null, userId);
		if (jestResult.getResponseCode() != 200) {
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(), 
					"id",
					"Unable to deactivate un user in ES",
					userId);
			throw new InvalidDataException(exceptionResponse);
		}
			
	}
	
	public boolean isEmailExist(SearchResult searchResult) {
		boolean flag = false;
		JsonObject jsonObject = searchResult.getJsonObject().getAsJsonObject("hits");
        try {
        	Object count = jsonObject.get("total");
        	int number = Integer.valueOf(count.toString());
        	if(number == 1)
        		flag = true;
        }catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}
	
	/*
	public boolean isEmailExist(SearchResult searchResult) {
		boolean flag = false;
		JsonObject userObj = searchResult.getJsonObject().getAsJsonObject("hits");
        JsonArray userResp = null;
        Gson gson = new Gson();
        Set emailSet = new HashSet<String>(); 
        try {
        	userResp = userObj.getAsJsonArray("hits");
			if(userResp.size() > 0) {
				for (JsonElement jsonElement : userResp) {
					JsonObject sourceObj = jsonElement.getAsJsonObject().getAsJsonObject("_source");
					String userJsonStr =  gson.toJson(sourceObj);
					User user =  new GsonBuilder()
						        .setDateFormat("dd-MMM-yyyy")
						        .create()
						        .fromJson(userJsonStr, User.class);
					if (!emailSet.add(user.getEmail())) 
						return true;
				}
			}
        }catch (Exception e) {
			e.printStackTrace();
		}
		return flag;
	}*/

}
