package com.intellectdesign.assignment.api.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.intellectdesign.assignment.api.model.RestError;
import com.intellectdesign.assignment.api.model.RestResponse;
import com.intellectdesign.assignment.api.model.User;

import io.searchbox.client.JestResult;
import io.searchbox.core.SearchResult;

@Service
public class APICommonUtility {

	@Autowired
	EmailValidator emailValidator;
	
	@Autowired
	ValidatorUtil validatorUtil;
	

	@SuppressWarnings("null")
	public RestResponse validateInputField(RestResponse restResponse, User user) {
		List<RestError> valErrorList = new ArrayList<RestError>();

		String firstName = user.getfName();
		String lastName = user.getlName();
		String email = user.getEmail();
		Date birthDate = user.getBirthDate();
		Number pincode = user.getPinCode();
		
		if( firstName == null  || firstName.trim().length() == 0 ) {

			RestError restError	= createRestErrorObject(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(),
					"fName",
					"First Name is mandatory."
					);
			valErrorList.add(restError);
		}
		if( lastName == null  || lastName.trim().length() == 0 ) {
			RestError restError	= createRestErrorObject(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(),
					"lName",
					"Last Name is mandatory."
					);
			valErrorList.add(restError);
		}
		if( email == null  || email.trim().length() == 0 ) {
			RestError restError	= createRestErrorObject(
					HttpStatus.UNPROCESSABLE_ENTITY.toString(),
					"email",
					"Email is mandatory."
					);	
			valErrorList.add(restError);
		}
		
		if(valErrorList.size() > 0 ) {
			//Return mandatory field validation error response
			restResponse = new RestResponse();
			restResponse.setResMsg("Manadatory input field error.");
			restResponse.setUserId(null);
			restResponse.setValErrors(valErrorList);
			return restResponse;
		}
		valErrorList.clear();
		
		valErrorList = validateFields(firstName, lastName, email, pincode, birthDate);
				
		if( valErrorList.size() > 0 ) {
			//Return validation error response
			restResponse = new RestResponse();
			restResponse.setResMsg("Input field validation error.");
			restResponse.setUserId(null);
			restResponse.setValErrors(valErrorList);
			return restResponse;
		}
		return restResponse;	
	}
	
	
	/**Convert Java object into json string
	 * @param obj
	 * @return String
	 */
	public String getJsonStringFromObject(Object obj){
		String jsonStr = null;
		ObjectMapper objectMapper = new ObjectMapper();
			try {
				jsonStr = objectMapper.writeValueAsString(obj);
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return jsonStr;
	}
	
	public String createRestResponseString(String code,String field, String message, String userId){
		RestResponse restResponse = null;
		String restErrorStr = null;
		List<RestError> valErrors = null;
		
		RestError restError = new RestError();
			restError.setCode(code);
			restError.setField(field);
			restError.setMessage(message);
			
		valErrors = new ArrayList<RestError>();	
		valErrors.add(restError);
		
		restResponse = new RestResponse();	
		restResponse.setResMsg(message);
		restResponse.setUserId(userId);
		restResponse.setValErrors(valErrors);
		
		restErrorStr = 	getJsonStringFromObject(restResponse);
		return restErrorStr;
	}
	
	public RestError createRestErrorObject(String code,String field, String message) {
		RestError restError = new RestError();
			restError.setCode(code);
			restError.setField(field);
			restError.setMessage(message);
		return restError;
	}
	
	public List<RestError> validateFields(String firstName, String lastName, String email, Number pinCode, Date birthDate) {
		List<RestError> valErrorList = new ArrayList<RestError>();
		
		if( firstName != null ) {
			if( !validatorUtil.validateUserName(firstName) ) {
				RestError restError	= createRestErrorObject(
						HttpStatus.UNPROCESSABLE_ENTITY.toString(),
						"fName",
						"First Name should be alphabetical."
						);	
				valErrorList.add(restError);
			}
		}
		if( lastName != null ) {
			if( !validatorUtil.validateUserName(lastName) ) {
				RestError restError	= createRestErrorObject(
						HttpStatus.UNPROCESSABLE_ENTITY.toString(),
						"lName",
						"Last Name should be alphabetical."
						);	
				valErrorList.add(restError);
				
			}
		}
		if( email != null ) {
			if( !emailValidator.validate(email) ) {
				RestError restError	= createRestErrorObject(
						HttpStatus.UNPROCESSABLE_ENTITY.toString(),
						"email",
						"Please enter valid email or email id.."
						);	
				valErrorList.add(restError);
			}
		}
		if( pinCode != null ) {
			if( !validatorUtil.validatePincode(pinCode) ) {
				RestError restError	= createRestErrorObject(
						HttpStatus.UNPROCESSABLE_ENTITY.toString(),
						"pinCode",
						"Please enter valid pincode."
						);	
				valErrorList.add(restError);
			}
		}
		if( birthDate != null ) {
			if( !validatorUtil.compateDates(birthDate)) {
				RestError restError	= createRestErrorObject(
						HttpStatus.UNPROCESSABLE_ENTITY.toString(),
						"birthDate",
						"Please enter valid birthDate."
						);	
				valErrorList.add(restError);
			}
		}
		
		return valErrorList;
	}
			
}
