package com.intellectdesign.assignment.api.util;

public enum CRUDConstants {
	
	CREATE("create"),
	READ("read"),
	UPDATE("update"),
	DELETE("delete");
	
	private String constants;
	
	CRUDConstants(String value){
		this.constants = value;
	}
	
	public String getCRUDConstants() {
		return constants;
	}
}
