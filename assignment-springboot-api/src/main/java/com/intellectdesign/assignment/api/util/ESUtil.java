package com.intellectdesign.assignment.api.util;

import java.io.IOException;

import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.intellectdesign.assignment.api.exception.ESClientException;
import com.intellectdesign.assignment.api.exception.InvalidDataException;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;
import io.searchbox.core.Delete;
import io.searchbox.core.Get;
import io.searchbox.core.Index;
import io.searchbox.core.Search;
import io.searchbox.core.SearchResult;
import io.searchbox.core.Update;

@Service
public class ESUtil {
	
	public static final String INDEX_NAME="user_index";
	
	public static final String TYPE="user";
	
	@Autowired
	JestClient jestClient;
	
	@Autowired
	APICommonUtility apiCommonUtil;
		
	public JestResult processESData(String operation, String data, String userId) throws IOException, ESClientException {
		JestResult jestResult = null;
		try {
			if(CRUDConstants.CREATE.toString().equalsIgnoreCase(operation)) {
				Index createDoc = new Index.Builder(data).index(INDEX_NAME).type(TYPE).id(userId).build();
				jestResult = jestClient.execute(createDoc);
			} else if (CRUDConstants.UPDATE.toString().equalsIgnoreCase(operation)) {
				Update updateDoc = new Update.Builder(data).index(INDEX_NAME).type(TYPE).id(userId).build();
				jestResult = jestClient.execute(updateDoc);
			} else if (CRUDConstants.DELETE.toString().equalsIgnoreCase(operation)) {
				if (userId != null) {
					Delete deleteDoc = new Delete.Builder(userId).index(INDEX_NAME).type(TYPE).build();
					jestResult = jestClient.execute(deleteDoc);
				}
			}else if(CRUDConstants.READ.toString().equalsIgnoreCase(operation)) {
				if (userId != null) {
					Get readDoc = new Get.Builder(INDEX_NAME, userId).type(TYPE).build();
					jestResult = jestClient.execute(readDoc);
				}
			}
		}catch (Exception e) {
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.SERVICE_UNAVAILABLE.toString(), 
					"",
					e.getMessage(),
					null);
			throw new ESClientException(exceptionResponse);
		}
		return jestResult;
	}
		
	public SearchResult searchIntoESData(String emailId) throws IOException, ESClientException {
		SearchResult searchResult = null;
		try {
			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			QueryBuilder qb = QueryBuilders
					.termQuery("email", emailId);
			searchSourceBuilder.query(qb);
			Search search = new Search.Builder(searchSourceBuilder.toString())
			                                // multiple index or types can be added.
			                                .addIndex(INDEX_NAME)
			                                .addType(TYPE)
			                                .build();
	
			searchResult = jestClient.execute(search);
		}catch (Exception e) {
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.SERVICE_UNAVAILABLE.toString(), 
					"",
					e.getMessage(),
					null);
			throw new ESClientException(exceptionResponse);
		}
		
		return searchResult;
	}
	
	/*
	 public SearchResult searchIntoESData(String emailId) throws IOException, ESClientException {
		SearchResult searchResult = null;
		try {
			SearchSourceBuilder searchSourceBuilder = new SearchSourceBuilder();
			QueryBuilder qb = QueryBuilders
//					.matchAllQuery();
					.matchQuery("email", emailId);
	//				.constantScoreQuery(QueryBuilders.termQuery("email", emailId));
			searchSourceBuilder.query(qb);
			Search search = new Search.Builder(searchSourceBuilder.toString())
			                                // multiple index or types can be added.
			                                .addIndex(INDEX_NAME)
			                                .addType(TYPE)
			                                .build();
	
			searchResult = jestClient.execute(search);
		}catch (Exception e) {
			String exceptionResponse = apiCommonUtil.createRestResponseString(
					HttpStatus.SERVICE_UNAVAILABLE.toString(), 
					"",
					e.getMessage(),
					null);
			throw new ESClientException(exceptionResponse);
		}
		
		return searchResult;
	}
	 */

}
