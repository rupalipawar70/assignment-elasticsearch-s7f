package com.intellectdesign.assignment.api.util;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Service;

@Service
public class RandomIDGenerator {
	
	/**This method give an unique id
	 * @return String
	 */
	public String generateRandomId() {
		String id = RandomStringUtils.randomAlphanumeric(5);
		return id;
	}	

}
