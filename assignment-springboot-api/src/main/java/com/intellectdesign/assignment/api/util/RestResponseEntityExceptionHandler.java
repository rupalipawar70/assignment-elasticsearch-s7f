package com.intellectdesign.assignment.api.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellectdesign.assignment.api.exception.ESClientException;
import com.intellectdesign.assignment.api.exception.InvalidDataException;
import com.intellectdesign.assignment.api.model.RestError;
import com.intellectdesign.assignment.api.model.RestResponse;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler{
	
	@Autowired
	ObjectMapper objectMapper;
	
	@ExceptionHandler({InvalidDataException.class })
	public ResponseEntity<RestResponse> handleInvalidDataException(Exception e, WebRequest request){
		
		RestResponse restResponse = null;
		try {
			restResponse = objectMapper.readValue(e.getMessage(), RestResponse.class);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return new ResponseEntity<RestResponse>(restResponse,HttpStatus.UNPROCESSABLE_ENTITY);
	}
	@ExceptionHandler({ESClientException.class })
	public ResponseEntity<RestResponse> handleESClientException(Exception e, WebRequest request){
		
		RestResponse restResponse = null;
		try {
			restResponse = objectMapper.readValue(e.getMessage(), RestResponse.class);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return new ResponseEntity<RestResponse>(restResponse,HttpStatus.SERVICE_UNAVAILABLE);
	}
	
	@ExceptionHandler({Throwable.class })
	public ResponseEntity<RestResponse> handleDefault(Throwable e, WebRequest request) {
		
		RestResponse restResponse = new RestResponse();
		restResponse.setResMsg("Internal API Error");
		
		List<RestError> valErrors = null;
		
		RestError restError = new RestError();
			restError.setCode(HttpStatus.INTERNAL_SERVER_ERROR.toString());
			restError.setField("");
			restError.setMessage(e.getMessage());
			
		valErrors = new ArrayList<RestError>();	
		valErrors.add(restError);
        return new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
	
}
