package com.intellectdesign.assignment.api.controller.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellectdesign.assignment.api.model.RestResponse;
import com.intellectdesign.assignment.api.model.User;
import com.intellectdesign.assignment.api.util.RandomIDGenerator;
import com.intellectdesign.assignment.api.util.RestAPIConstants;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class UserControllerTest{
	
	@Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;
    private ObjectMapper objectMapper;
    
    @Autowired
    RandomIDGenerator randomIDGenerator;
    
    String subUrl = RestAPIConstants.USER_CONTROLLER_V1.getRestAPIConstants();
    public static final DateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
 

    @Before
    public void setup () {
        DefaultMockMvcBuilder builder = MockMvcBuilders.webAppContextSetup(this.wac);
        this.mockMvc = builder.build();
        objectMapper = new ObjectMapper();
    }
    
    public String createUser() {
    	String createUserRequest = null; 
    	String createUserResponse = null;
    	ResultActions resultActions = null;
    	RestResponse restResponse = null;
    	String userId = null;
    	try {
        	User user = new User();
    			user.setfName("Rupali");
    			user.setlName("Pawar");
    			user.setEmail("rupali.pawar" + randomIDGenerator.generateRandomId()+"@intellectdesign.com");
    			user.setBirthDate(df.parse("20-Dec-1990"));
    			user.setPinCode(421260);
    			user.setActive(true);
			createUserRequest = objectMapper.writeValueAsString(user); 
			
			MockHttpServletRequestBuilder builder =
                     MockMvcRequestBuilders.post(subUrl +"/create")
                          .contentType(MediaType.APPLICATION_JSON)
                          .content(createUserRequest);
			
			resultActions = this.mockMvc.perform(builder)
			      .andExpect(MockMvcResultMatchers.status().isCreated());			
      
			createUserResponse = resultActions.andReturn().getResponse().getContentAsString();
			System.out.println("+++++++++++++++++++++++" + createUserResponse);
			assertNotNull("createUserResponse is null ",createUserResponse);
    		
			restResponse = objectMapper.readValue(createUserResponse, RestResponse.class);
			userId = restResponse.getUserId();

			//Test createUser method response
    		assertNotNull("UserId is null", userId);
    		assertEquals("User created successfully.",restResponse.getResMsg());
    		assertNull("valErrors is not null",restResponse.getValErrors());
    		
    	}catch (Exception e) {
			e.printStackTrace();
		}
    	return userId;
    }
    
    @Test
    public void createUserWithValidData() {
    	String userId = null;
    	try {
    		userId = createUser();
    	} catch (Exception e) {
			e.printStackTrace();
		}	
    }
    
    @Test
    public void createUserWithInvalidData() {
    	String createUserRequest = null; 
    	String createUserResponse = null;
    	ResultActions resultActions = null;
    	RestResponse restResponse = null;
    	String userId = null;
    	try {
        	User user = new User();
    			user.setfName("Rupali123");
    			user.setlName("Pawar12678");
    			user.setEmail("rupali.pawar" + randomIDGenerator.generateRandomId()+"@intellectdesign.com");
    			user.setBirthDate(df.parse("20-Dec-1990"));
    			user.setPinCode(42126056);
    			user.setActive(true);
			createUserRequest = objectMapper.writeValueAsString(user); 
			
			MockHttpServletRequestBuilder builder =
                     MockMvcRequestBuilders.post(subUrl +"/create")
                          .contentType(MediaType.APPLICATION_JSON)
                          .content(createUserRequest);
			
			resultActions = this.mockMvc.perform(builder)
			      .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity());			
      
			createUserResponse = resultActions.andReturn().getResponse().getContentAsString();
			System.out.println("+++++++++++++++++++++++" + createUserResponse);
			assertNotNull("createUserResponse is null ",createUserResponse);
    		
			restResponse = objectMapper.readValue(createUserResponse, RestResponse.class);
			userId = restResponse.getUserId();
			
    		assertNull("UserId is not null ", userId);	
    	}catch (Exception e) {
			e.printStackTrace();
		}
   }
    
    
    @Test
    public void updateUserWithValidData() {
    	User newUser = null;
    	String userId = null;
    	String updateUserRequest = null; 
    	String updateUserResponse = null;
    	ResultActions resultActions = null;
    	RestResponse restResponse = null;
    	try {
    		userId = createUser();
    		newUser = new User();
    		newUser.setPinCode(410210);
    		newUser.setBirthDate(df.parse("20-Dec-1980"));
    		
    		updateUserRequest = objectMapper.writeValueAsString(newUser); 
			
			MockHttpServletRequestBuilder builder =
                     MockMvcRequestBuilders.patch(subUrl +"/update/"+userId)
                          .contentType(MediaType.APPLICATION_JSON)
                          .content(updateUserRequest);
			
			resultActions = this.mockMvc.perform(builder)
			      .andExpect(MockMvcResultMatchers.status().isOk());			
      
			updateUserResponse = resultActions.andReturn().getResponse().getContentAsString();
			System.out.println("+++++++++++++++++++++++" + updateUserResponse);
			assertNotNull("createUserResponse is null ",updateUserResponse);
    			
    		restResponse = objectMapper.readValue(updateUserResponse, RestResponse.class);
    		assertNotNull("Response Updated User entity is null ", restResponse);
    		
    		//Test updateUser method response
    		assertNotNull("UserId is null", userId);
    		assertEquals("User updated successfully.",restResponse.getResMsg());
    		assertNull("valErrors is not null",restResponse.getValErrors());
    	}catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    @Test
    public void updateUserWithInvalidData() {
    	User newUser = null;
    	String userId = null;
    	String updateUserRequest = null; 
    	ResultActions resultActions = null;
    	try {
    		userId = createUser();
    		newUser = new User();
    		newUser.setPinCode(123654892);
    		newUser.setBirthDate(df.parse("20-Dec-1970"));
    		
    		updateUserRequest = objectMapper.writeValueAsString(newUser); 
			
			MockHttpServletRequestBuilder builder =
                     MockMvcRequestBuilders.patch(subUrl +"/update/"+userId)
                          .contentType(MediaType.APPLICATION_JSON)
                          .content(updateUserRequest);
			
			resultActions = this.mockMvc.perform(builder)
			      .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity());			
   		
    	}catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    @Test
    public void disableUserWithValidData() {
    	String userId = null;
    	ResultActions resultActions = null;
    	String disableUserResponse = null;
    	RestResponse restResponse = null;
    	try {
    		userId = createUser();
    		
			MockHttpServletRequestBuilder builder =
                     MockMvcRequestBuilders.delete(subUrl +"/disable/"+userId)
                          .contentType(MediaType.APPLICATION_JSON);
            			
			resultActions = this.mockMvc.perform(builder)
			      .andExpect(MockMvcResultMatchers.status().isOk());			
      
			disableUserResponse = resultActions.andReturn().getResponse().getContentAsString();
			System.out.println("+++++++++++++++++++++++" + disableUserResponse);
			
			assertNotNull("disableUserResponse is null ",disableUserResponse);
			
			restResponse = objectMapper.readValue(disableUserResponse, RestResponse.class);
			
			//Test disableUser method response
    		assertNotNull("UserId is null", userId);
    		assertEquals("User is deactivated.",restResponse.getResMsg());
    		assertNull("valErrors is not null",restResponse.getValErrors());
		
    	}catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    @Test
    public void disableUserWithInvalidData() {
    	ResultActions resultActions = null;
    	try {
    		MockHttpServletRequestBuilder builder =
                     MockMvcRequestBuilders.delete(subUrl +"/disable/test")
                          .contentType(MediaType.APPLICATION_JSON);
            resultActions = this.mockMvc.perform(builder)
			      .andExpect(MockMvcResultMatchers.status().isUnprocessableEntity());			
    	}catch (Exception e) {
			e.printStackTrace();
		}
    }
}
